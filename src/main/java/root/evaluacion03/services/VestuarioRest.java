/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion03.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evaluacion03.dao.VestuarioJpaController;
import root.evaluacion03.dao.exceptions.NonexistentEntityException;
import root.evaluacion03.entity.Vestuario;

/**
 *
 * @author sofia
 */
@Path("vestuario")

public class VestuarioRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Response listarVestuario() {

        VestuarioJpaController dao = new VestuarioJpaController();

        List<Vestuario> vestuario = dao.findVestuarioEntities();
        return Response.ok(200).entity(vestuario).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Vestuario vestuario) {
        VestuarioJpaController dao = new VestuarioJpaController();
        try {
            dao.create(vestuario);
        } catch (Exception ex) {
            Logger.getLogger(VestuarioRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(vestuario).build();

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Vestuario vestuario) {

        VestuarioJpaController dao = new VestuarioJpaController();
        try {
            dao.edit(vestuario);
        } catch (Exception ex) {
            Logger.getLogger(VestuarioRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(vestuario).build();

    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar) {

        VestuarioJpaController dao = new VestuarioJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(VestuarioRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("vestuario eliminado").build();

    }

    @GET
    @Path("/{idConsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorId(@PathParam("idConsultar") String idConsultar) {
        
        VestuarioJpaController dao = new VestuarioJpaController();
        
        Vestuario vestuario = dao.findVestuario(idConsultar);
        
        return Response.ok(200).entity(vestuario).build();

    }
}
