/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion03.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sofia
 */
@Entity
@Table(name = "vestuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vestuario.findAll", query = "SELECT v FROM Vestuario v"),
    @NamedQuery(name = "Vestuario.findByNombre", query = "SELECT v FROM Vestuario v WHERE v.nombre = :nombre"),
    @NamedQuery(name = "Vestuario.findByGenero", query = "SELECT v FROM Vestuario v WHERE v.genero = :genero"),
    @NamedQuery(name = "Vestuario.findByColor", query = "SELECT v FROM Vestuario v WHERE v.color = :color"),
    @NamedQuery(name = "Vestuario.findByTemporada", query = "SELECT v FROM Vestuario v WHERE v.temporada = :temporada")})
public class Vestuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "genero")
    private String genero;
    @Size(max = 2147483647)
    @Column(name = "color")
    private String color;
    @Size(max = 2147483647)
    @Column(name = "temporada")
    private String temporada;

    public Vestuario() {
    }

    public Vestuario(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTemporada() {
        return temporada;
    }

    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vestuario)) {
            return false;
        }
        Vestuario other = (Vestuario) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.evaluacion03.entity.Vestuario[ nombre=" + nombre + " ]";
    }
    
}
