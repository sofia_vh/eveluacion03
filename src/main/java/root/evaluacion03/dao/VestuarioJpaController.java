/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion03.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.evaluacion03.dao.exceptions.NonexistentEntityException;
import root.evaluacion03.dao.exceptions.PreexistingEntityException;
import root.evaluacion03.entity.Vestuario;

/**
 *
 * @author sofia
 */
public class VestuarioJpaController implements Serializable {

    public VestuarioJpaController() {
       
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_SOLEMNE_03");
    
    

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Vestuario vestuario) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(vestuario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVestuario(vestuario.getNombre()) != null) {
                throw new PreexistingEntityException("Vestuario " + vestuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Vestuario vestuario) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            vestuario = em.merge(vestuario);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = vestuario.getNombre();
                if (findVestuario(id) == null) {
                    throw new NonexistentEntityException("The vestuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vestuario vestuario;
            try {
                vestuario = em.getReference(Vestuario.class, id);
                vestuario.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The vestuario with id " + id + " no longer exists.", enfe);
            }
            em.remove(vestuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Vestuario> findVestuarioEntities() {
        return findVestuarioEntities(true, -1, -1);
    }

    public List<Vestuario> findVestuarioEntities(int maxResults, int firstResult) {
        return findVestuarioEntities(false, maxResults, firstResult);
    }

    private List<Vestuario> findVestuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Vestuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Vestuario findVestuario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Vestuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getVestuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Vestuario> rt = cq.from(Vestuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
