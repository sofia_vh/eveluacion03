<%-- 
    Document   : index
    Created on : 17-jun-2021, 16:24:23
    Author     : sofia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <br>
        <br>
        <table border="1px" align="center">
            <tr>
                <th colspan="3">Endpoints Api Vestuario</th>

            </tr>
            <tr>
                <td>Lista Vestuario</td>
                <td>GET</td>
                <td>https://solemne03.herokuapp.com/api/vestuario</td>

            </tr>
            <tr>
                <td>Lista Vestuario por id</td>
                <td>GET</td>
                <td>https://solemne03.herokuapp.com/api/vestuario/{id}</td>
            </tr>

            <tr>
                <td>Crear Vestuario</td>
                <td>POST</td>
                <td>https://solemne03.herokuapp.com/api/vestuario</td>
            </tr>

            <tr>
                <td>Actualizar Vestuario</td>
                <td>PUT</td>
                <td>https://solemne03.herokuapp.com/api/vestuario</td>
            </tr>

            <tr>
                <td>Eliminar Vestuario</td>
                <td>DELETE</td>
                <td>https://solemne03.herokuapp.com/api/vestuario</td>
            </tr>

        </table>

    </body>
</html>
